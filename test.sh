# 定义一个数组，包含所有 CDN 的 URL
cdn_urls=("https://gh.api.99988822266.xyz/" "https://shrill-pond-3222e81.hunsh.workers.dev/")

# 定义一个函数，用于检测哪个 CDN 可用
check_cdn() {
  local o_url=$1
  # 循环遍历 CDN URL 数组
  for cdn_url in "${cdn_urls[@]}"; do
    # 尝试从 CDN 下载文件
    if curl -L -k "$cdn_url$o_url" | grep -q "success"; then
      # 如果下载成功，则返回 CDN URL
      export cdn_success_url="$cdn_url"
      return
    fi
  done
  # 如果所有 CDN 都失败，则返回空字符串
  export cdn_success_url=""
}

# 检测可用的 CDN
check_cdn "https://raw.githubusercontent.com/spiritLHLS/ecs/main/back/test"
# 如果检测到 CDN 可用，输出 CDN URL
if [ -n "$cdn_success_url" ]; then
  echo "Available CDN: $cdn_success_url"
else
  echo "No CDN available"
fi

curl -L -k "${cdn_success_url}https://raw.githubusercontent.com/spiritLHLS/ecs/main/CHANGELOG.md" -o CHANGELOG.md
